package vn.vccorp.bigdata.test4;



/**
 * Author: Niklas Svensson (nks.gnu@gmail.com)
 */

import java.util.Comparator;

public class SortComp implements Comparator<Student> {
	
	/**
	 * Compares two students points and returns the opposite value
	 * to help the sorting algorithm.
	 */
	public int compare(Student x, Student y) {
		return y.getPoints() - x.getPoints();
	}
}
