package vn.vccorp.bigdata.test4;

/**
 * Author: Niklas Svensson (nks.gnu@gmail.com)
 */

public class Student implements Comparable<Object> {
	
	private String fn, ln;
	private int points;
	
	/**
	 * Creates a student object.
	 * The parameters usage is to determine the students name.
	 */
	public Student(String ln, String fn) {
		this.ln = ln;
		this.fn = fn;
	}
	
	/**
	 * Returns the students points.
	 */
	public int getPoints() {
		return points;
	}
	
	/**
	 * Adds a certain point to the student.
	 * The parameters usage is to determine what value should
	 * be added to the student.
	 */
	public void addPoints(int p) {
		points += p;
	}
	
	/**
	 * Determines if two students are the same.
	 * The parameters usage is to determine what student to compare with.
	 * Returns the result.
	 */
	public boolean equals(Object x) {
		return this.compareTo(x) == 0;
	}
	
	/**
	 * Compares two students.
	 * The parameters usage is to determine what student to compare with.
	 * Returns the result.
	 */
	public int compareTo(Object x) {
		int cmp = ln.compareTo(((Student)x).ln);
        if (cmp != 0) {
            return cmp;
        }
        return fn.compareTo(((Student)x).fn);
	}
	
	/**
	 * Is used to be able to print a students name when iterating.
	 * Returns the students name.
	 */
	public String toString() {
		return ln + " " + fn;
	}
}
