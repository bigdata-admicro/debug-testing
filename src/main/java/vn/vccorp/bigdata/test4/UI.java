package vn.vccorp.bigdata.test4;

/**
 * Author: Niklas Svensson (nks.gnu@gmail.com)
 */

import java.util.*;
import java.io.*;

public class UI {

	/**
	 * The programs main-method. The parameters usage is to determine the file
	 * of the register.
	 */
	public static void main(String args[]) {
		File file = new File("resource/register.txt");
		Register reg = new Register(file);
		int cmd = 0;
		do {
			printMenu();
			do {
				cmd = getCmd();
			} while ((cmd > 10 || cmd < 1));
			callCmd(cmd, reg);
		} while (cmd != 10);
	}

	/**
	 * Prints the menu.
	 */
	private static void printMenu() {
		System.out.println("\n1.\t Add student");
		System.out.println("2.\t Register finished class");
		System.out.println("3.\t Print the number of students in the register");
		System.out.println("4.\t Add class");
		System.out.println("5.\t Print info about a students grades");
		System.out.println("6.\t Print info about a students points");
		System.out.println("7.\t Print a list of students who has more than x points");
		System.out.println("8.\t Print a list of x students with most points");
		System.out.println("9.\t Update the register from file");
		System.out.println("10.\t Save and exit\n");
	}

	/**
	 * Reads an integer from keyboard. Returns the inputed integer.
	 */
	private static int getCmd() {
		Scanner in = new Scanner(System.in);
		int cmd = Integer.MAX_VALUE;
		while (cmd == Integer.MAX_VALUE) {
			try {
				System.out.println("Enter command:");
				cmd = Integer.parseInt(in.nextLine());
			} catch (NumberFormatException e) {
				System.out.println("Only integers are allowed commands\n");
			}
		}
		return cmd;
	}

	/**
	 * Calls for the function which corresponds the the menu. The parameters
	 * usage is to determine what menu option was chosen.
	 */
	private static void callCmd(int cmd, Register reg) {
		switch (cmd) {
		case 1:
			reg.addStudent();
			break;
		case 2:
			reg.regClassToStudent();
			break;
		case 3:
			reg.printNbrOfStudents();
			break;
		case 4:
			reg.addClass();
			break;
		case 5:
			reg.printGrades();
			break;
		case 6:
			reg.printPoints();
			break;
		case 7:
			reg.listOfStudsWithCertainPoints();
			break;
		case 8:
			reg.listOfCertainStudsWithMostPoints();
			break;
		case 9:
			reg.updateRegFromFile();
			break;
		case 10:
			reg.save();
			break;
		}
	}
}
