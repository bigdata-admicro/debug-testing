package vn.vccorp.bigdata.test4;

/**
 * Author: Niklas Svensson (nks.gnu@gmail.com)
 */

import java.util.*;
import java.io.*;

public class Register {

	private TreeMap<Student, HashMap<String, Integer>> students;
	private HashMap<String, Integer> classes;
	private File file;

	/**
	 * Creates a register object. The parameters usage it to determine what file
	 * to save the register to.
	 */
	public Register(File file) {
		this.file = file;
		students = new TreeMap<Student, HashMap<String, Integer>>();
		classes = new HashMap<String, Integer>();
		readFile();
	}

	/**
	 * Reads the register. O(n) = n*log(n).
	 */
	private void readFile() {
		Scanner in = null;
		try {
			in = new Scanner(file);
		} catch (FileNotFoundException e) {
			System.out.println("The file: " + file + " does not exist.");
			System.exit(0);
		}
		Scanner lineScanner;
		String l = in.nextLine();
		char q = l.charAt(0);
		while (q != '#') {
			lineScanner = new Scanner(l);
			classes.put(lineScanner.next(), lineScanner.nextInt());
			l = in.nextLine();
			q = l.charAt(0);
		}
		l = in.nextLine();
		while (in.hasNextLine()) {
			lineScanner = new Scanner(l);
			Student s = new Student(lineScanner.next(), lineScanner.next());
			students.put(s, new HashMap<String, Integer>());
			HashMap<String, Integer> m = students.get(s);
			l = in.nextLine();
			q = l.charAt(0);
			while (q != '#') {
				lineScanner = new Scanner(l);
				String clss = lineScanner.next();
				int pnt = lineScanner.nextInt();
				m.put(clss, pnt);
				s.addPoints(classes.get(clss));
				l = in.nextLine();
				q = l.charAt(0);
			}
			if (in.hasNext()) {
				l = in.nextLine();
			}
		}

	}

	/**
	 * Adds a student to the register. O(n) = log(n).
	 */
	public void addStudent() {
		Scanner in = new Scanner(System.in);
		System.out.println("Lastname:");
		String ln = in.nextLine();
		System.out.println("Firstname:");
		String fn = in.nextLine();
		Student s = new Student(ln, fn);
		if (students.containsKey(s)) {
			students.put(s, new HashMap<String, Integer>());
		} else {
			System.out.println("The student you entered does already exist in the register.");
		}

	}

	/**
	 * Registers a finished class for a student. O(n) = log(n).
	 */
	public void regClassToStudent() {
		Scanner in = new Scanner(System.in);
		boolean name = false;
		String c = null;
		String ln = null;
		String fn = null;
		Student s = null;
		HashMap<String, Integer> m = null;
		while (!name) {
			System.out.println("Lastname:");
			ln = in.nextLine();
			System.out.println("Firstname:");
			fn = in.nextLine();
			s = new Student(ln, fn);
			m = students.get(s);
			if (m != null) {
				name = true;
			} else {
				System.out.println("The student you are looking for does not exist.");
			}
		}
		boolean classcode = false;
		while (!classcode) {
			System.out.println("Classcode:");
			c = in.nextLine();
			if (classes.containsKey(c)) {
				classcode = true;
			} else {
				System.out.println("The class you are looking for does not exist.");
			}
		}
		int b = 0;
		// grade between 3-5
		while (b < 5 || b > 3) {
			b = getCmd("Enter grade:");
		}
		m.put(c, b);
		s.addPoints(classes.get(c));

	}

	/**
	 * Prints the number of students in the register. O(n) = 1.
	 */
	public void printNbrOfStudents() {
		System.out.println("The number of students in the register are: " + students.size());
	}

	/**
	 * Adds a class to the register. O(n) = 1.
	 */
	public void addClass() {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter classcode:");
		String c = in.nextLine();
		if (!classes.containsKey(c)) {
			int p = getCmd("Enter the number of points:");
			classes.put(c, p);
		} else {
			System.out.println("The class does already exist.");
		}

	}

	/**
	 * Prints a students finished classes and grades. O(n) = log(n).
	 */
	public void printGrades() {
		Scanner in = new Scanner(System.in);
		boolean name = false;
		String ln = null;
		String fn = null;
		HashMap<String, Integer> m = null;
		while (!name) {
			System.out.println("Lastname:");
			ln = in.nextLine();
			System.out.println("Firstname:");
			fn = in.nextLine();
			m = students.get(new Student(ln, fn));
			if (m != null) {
				name = true;
			}
		}
		Iterator<Integer> v = m.values().iterator();
		Iterator<String> k = m.keySet().iterator();
		for (int i = 0; i < m.size(); i++) {
			System.out.println(k.next() + " " + v.next());
		}

	}

	/**
	 * Prints a students points. O(n) = log(n).
	 */
	public void printPoints() {
		Scanner in = new Scanner(System.in);
		boolean name = false;
		String ln = null;
		String fn = null;
		Student s = null;
		HashMap<String, Integer> m = null;
		while (!name) {
			System.out.println("Lastname:");
			ln = in.nextLine();
			System.out.println("Firstname:");
			fn = in.nextLine();
			s = new Student(ln, fn);
			m = students.get(s);
			if (m != null) {
				name = true;
			}
		}

		System.out.println("The number of points are: " + s.getPoints());
	}

	/**
	 * Prints a list of students with at least a certain point. Alphabetically
	 * ordered. O(n) = n.
	 */
	public void listOfStudsWithCertainPoints() {
		int x = getCmd("Enter the number of minimum points:");
		Iterator<Student> k = students.keySet().iterator();
		for (int i = 0; i < students.size(); i++) {
			Student s = ((Student) k.next());
			if (s.getPoints() >= x) {
				System.out.println(s + " " + s.getPoints());
			}
		}
	}

	/**
	 * Prints a list av a certain number of students with most points. Sorted in
	 * descending order. O(n) = n*log(n).
	 */
	public void listOfCertainStudsWithMostPoints() {
		int x = getCmd("Enter the number of students:");
		Set<Student> set = students.keySet();
		Student[] lista = new Student[set.size()];
		set.toArray(lista);
		SortComp comp = new SortComp();
		Arrays.sort(lista, comp);
		for (int i = 0; i < x; i++) {
			if (i < set.size()) {
				System.out.println(lista[i] + " " + lista[i].getPoints());
			}
		}
	}

	/**
	 * Updates the register with results of an exam from a file. O(n) = n.
	 */
	public void updateRegFromFile() {
		System.out.println("Enter the name of the file:");
		Scanner read = new Scanner(System.in);
		File update = new java.io.File("reesource/" + read.nextLine());
		Scanner in = null;
		boolean really = true;
		try {
			in = new Scanner(update);
		} catch (FileNotFoundException e) {
			System.out.println("The file you entered does not exist.");
			really = false;
		}
		if (really) {
			String theClass = in.nextLine();
			if (classes.containsKey(theClass)) {
				Iterator<Student> studItr = students.keySet().iterator();
				Iterator<HashMap<String, Integer>> classItr = students.values().iterator();
				while (in.hasNext()) {
					String s = in.nextLine();
					Scanner l = new Scanner(s);
					Student stud = new Student(l.next(), l.next());
					int grade = l.nextInt();
					Student studItrVal = (Student) studItr.next();
					while (!stud.equals(studItrVal)) {
						studItrVal = (Student) studItr.next();
						classItr.next();
					}
					((HashMap<String, Integer>) classItr.next()).put(theClass, grade);
					stud.addPoints(classes.get(theClass));
				}
			} else {
				System.out.println("The class you tried to update does not exist.");
			}
		}

	}

	/**
	 * Saves the register to file. O(n) = n.
	 */
	public void save() {
		PrintWriter out = null;
		try {
			out = new PrintWriter(file);
		} catch (FileNotFoundException e) {

		}
		Iterator<Student> studItr = students.keySet().iterator();
		Iterator<HashMap<String, Integer>> classItr = students.values().iterator();
		Iterator<String> classSetItr = classes.keySet().iterator();
		Object temp = null;
		while (classSetItr.hasNext()) {
			temp = classSetItr.next();
			out.print(temp + " ");
			out.println(classes.get(temp));
		}
		out.println("#");
		while (studItr.hasNext()) {
			out.println(studItr.next());
			HashMap<String, Integer> m = (HashMap<String, Integer>) classItr.next();
			Iterator<String> studClassItr = m.keySet().iterator();
			while (studClassItr.hasNext()) {
				temp = studClassItr.next();
				out.print(temp + " ");
				out.println(m.get(temp));
			}
			out.println("#");
		}
		out.close();
	}

	/**
	 * Reads an integer from keyboard. Returns the inputed integer.
	 */
	private static int getCmd(String s) {
		Scanner in = new Scanner(System.in);
		int cmd = Integer.MAX_VALUE;
		while (cmd == Integer.MAX_VALUE) {
			try {
				System.out.println(s);
				cmd = Integer.parseInt(in.nextLine());
			} catch (NumberFormatException e) {
				System.out.println("Only integers are allowed commands\n");
			}
		}

		return cmd;
	}
}
