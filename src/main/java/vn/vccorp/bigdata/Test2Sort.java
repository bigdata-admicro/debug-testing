package vn.vccorp.bigdata;

/**
 * Hãy tìm và sửa lỗi của hàm sort dưới. Lưu ý: không sửa hàm main và không thay
 * đổi cấu trúc của thuật toán sort
 * 
 * @author ngocvm
 *
 */
public class Test2Sort {
	public static void main(String a[]) {
		int[] arr1 = { 10, 2, 34, 1, 56, 7, 67, 88, 42 };
		int[] arr2 = sort(arr1);
		for (int i : arr2) {
			System.out.print(i);
			System.out.print(", ");
		}
	}

	public static int[] sort(int[] input) {
		int temp;
		for (int i = 1; i < input.length; i++) {
			for (int j = i; j > 0; j++) {
				if (input[j] < input[j + 1]) {
					temp = input[j];
					input[j] = input[j + 1];
					input[j + 1] = temp;
				}
			}
		}
		return input;
	}

}
