package vn.vccorp.bigdata;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;

@Retention(RetentionPolicy.RUNTIME)
@interface MyAnnoFisrt {
	String key();

	String value();
}

@Retention(RetentionPolicy.RUNTIME)
@interface MyAnnoSecond {
	int count();
}

/**
 * Sửa lỗi khi sử dụng Annotation sau. Lưu ý: Không được sửa hàm main
 * 
 * @author ngocvm
 *
 */
public class Test3Annotation {
	@MyAnnoSecond(count = 20)
	@MyAnnoFisrt(key = "site", value = "dantri.com.vn")
	public void myAnnotationTestMethod() throws Exception {

		Class<? extends Test3Annotation> cls = this.getClass();
		Method mth = cls.getMethod("Annotation Test Method");
		Annotation[] anns = mth.getAnnotations();
		for (Annotation an : anns) {
			System.out.println(an);
		}

	}

	public static void main(String a[]) throws Exception {

		Test3Annotation mat = new Test3Annotation();
		mat.myAnnotationTestMethod();
	}
}
