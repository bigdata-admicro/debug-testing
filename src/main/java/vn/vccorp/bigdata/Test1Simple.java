package vn.vccorp.bigdata;

import java.util.ArrayList;
import java.util.List;

public class Test1Simple {
	/**
	 * Hàm trả lại List<Integer> từ chuỗi string có dạng 12;55;93;08, bỏ qua các
	 * phần tử sai định dạng. Ví dụ : chuỗi nhập là 1,a,21 thì trả lại list
	 * [1,21] bỏ qua giá trị sai 'a'
	 * 
	 * @param sinput
	 * @param separator
	 * @return
	 */
	public static List<Integer> parseIntegers(String sinput, String separator) {
		List<Integer> result = new ArrayList<Integer>();

		// parsing data
		String[] split = sinput.split(separator);
		for (int i = 1; i <= split.length;) {
			result.add(Integer.parseInt(split[i++]));
		}

		return result;
	}

	/**
	 * Yêu cầu:
	 * <li>Không sửa hàm main
	 * <li>Chỉ được sửa hàm parseIntegers để loại bỏ toàn bộ bug có xuất hiện
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// Sử dụng parseIntegers để phân tích các xâu sau:
		List<Integer> result = new ArrayList<>();
		result.addAll(parseIntegers("4,6,7", ","));
		result.addAll(parseIntegers("5,,3", ","));
		result.addAll(parseIntegers("5,1.2,3", ","));
		result.addAll(parseIntegers("5,3", null));
		result.addAll(parseIntegers(null, null));
	}
}
